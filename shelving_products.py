"""sys used for max number"""
import sys

"""Grid size below - to be adjusted manually."""
grid_size = (3, 2, 2)


def process_input(grid_size):
    """Finds coordinates for input product.
    The coordinates are calculated using Manhattan distance algorithm"""
    types = []
    columns, grid_occupancy = __form_grid(grid_size)
    print('Enter empty strings for object ID and object type to exit the application.\n')

    while True:
        object_id_input = input('Enter object ID: ')
        if object_id_input:
            try:
                object_id = int(object_id_input)
            except ValueError:
                print('ID must be a number. Please try again')
                continue

        object_type = input('Enter object type: ')
        if not object_id_input:
            if not object_type:
                break
            else:
                print('Product type is empty. Please try again')
                continue

        type_index = __find_type_index(object_type, types)
        non_full_column_index = __find_non_full_column_index(columns, grid_size, object_type)

        if non_full_column_index is not None:
            last_object_position = types[type_index]['last_object_position']
            grid_coordinates = __find_closest_cell_to_target_in_column(
                grid_occupancy, non_full_column_index, last_object_position)
        else:
            empty_column_index = None
            for index, column in enumerate(columns):
                if column['type'] is None:
                    empty_column_index = index
                    break

            if empty_column_index is None:
                print('No more empty columns are available!')
                continue

            if type_index is None:
                type_index = len(types)
                types.append({'type': object_type, 'last_object_position': None})
                grid_coordinates = (empty_column_index, 0, 0)
            else:
                last_object_position = types[type_index]['last_object_position']
                grid_coordinates = __find_closest_cell_to_target_in_column(
                    grid_occupancy, empty_column_index, last_object_position)

            columns[grid_coordinates[0]]['type'] = object_type

        grid_occupancy[grid_coordinates[0]][grid_coordinates[1]][grid_coordinates[2]] = True
        types[type_index]['last_object_position'] = grid_coordinates
        columns[grid_coordinates[0]]['occupied'] += 1
        print('Object #' + str(object_id) + ' added to position: ' + str(grid_coordinates))


def __find_non_full_column_index(columns, grid_size, object_type):
    non_full_column_index = None
    for index, column in enumerate(columns):
        if column['type'] == object_type and column['occupied'] < (grid_size[1] * grid_size[2]):
            non_full_column_index = index
            break
    return non_full_column_index


def __find_type_index(object_type, types):
    type_index = None
    for index, type_entry in enumerate(types):
        if type_entry['type'] == object_type:
            type_index = index
            break
    return type_index


def __form_grid(grid_size):
    columns = [{'type': None, 'occupied': 0} for i in range(grid_size[0])]
    grid_occupancy = [[[False for z in range(grid_size[2])] for y in range(
        grid_size[1])] for x in range(grid_size[0])]
    return columns, grid_occupancy


def __find_closest_cell_to_target_in_column(grid_occupancy, column, target):
    closest_cell = None
    minimum_distance = sys.maxsize

    for y in range(grid_size[1]):
        for z in range(grid_size[2]):
            if not grid_occupancy[column][y][z]:
                distance = abs(column - target[0]) + abs(y - target[1]) + abs(z - target[2])
                if distance < minimum_distance:
                    closest_cell = (column, y, z)
                    minimum_distance = distance
    return closest_cell

process_input((grid_size))
